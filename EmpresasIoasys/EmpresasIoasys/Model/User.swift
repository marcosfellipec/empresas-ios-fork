//
//  User.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

struct User: Codable {
    var investor: String
    var enterprise: Enterprise?
    var success: Bool

    init() {
        self.investor = ""
        self.enterprise = Enterprise()
        self.success = false
    }
}
