//
//  Portfolio.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

struct Portfolio: Codable {
    var enterprisesNumber: Int
    var enterprises: [Enterprise]?
    
    enum CodingKeys: String, CodingKey {
        case enterprisesNumber = "enterprises_number"
        case enterprises = "enterprises"
    }
}
