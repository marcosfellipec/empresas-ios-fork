//
//  Investor.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

struct InvestorNested: Codable {
    var investor: Investor
}

struct Investor: Codable {
    var id: Int
    var investorName: String
    var email: String
    var city: String
    var balance: Int
    var photo: String?
    var portfolio: Portfolio
    var portfolioValue: Int
    var firtAccess: Bool
    var superAngel: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case investorName = "investor_name"
        case email = "email"
        case city = "city"
        case balance = "balance"
        case photo = "photo"
        case portfolio = "portfolio"
        case portfolioValue = "portfolio_value"
        case firtAccess = "first_access"
        case superAngel = "super_angel"
    }
    
}
