//
//  Enterprise.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

struct Enterprises: Codable {
    var enterprises: [Enterprise]
}

struct Enterprise: Codable {
    var id: Int
    var emailEnterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var ownEnterprise: Bool
    var enterpriseName: String
    var photo: String?
    var description: String
    var city: String
    var country: String
    var value: Int
    var sharePrice: Int
    var enterpriseType: EnterpriseType
    
    enum CodingKeys: String, CodingKey {
       
        case id = "id"
        case emailEnterprise = "email_enterprise"
        case facebook = "facebook"
        case twitter = "twitter"
        case linkedin = "linkedin"
        case phone = "phone"
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case photo = "photo"
        case description = "description"
        case city = "city"
        case country = "country"
        case value = "value"
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
    
    init() {
        self.id = 0
        self.emailEnterprise = ""
        self.facebook = ""
        self.twitter = ""
        self.linkedin = ""
        self.phone = ""
        self.ownEnterprise = false
        self.enterpriseName = ""
        self.photo = ""
        self.description = ""
        self.city = ""
        self.country = ""
        self.value = 0
        self.sharePrice = 0
        self.enterpriseType = EnterpriseType()
    }
    
}
