//
//  EnterpriseType.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

struct EnterpriseType: Codable {
    var id: Int
    var enterpriseTypeName: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case enterpriseTypeName = "enterprise_type_name"
    }
    
    init() {
        self.id = 0
        self.enterpriseTypeName = ""
    }
}
