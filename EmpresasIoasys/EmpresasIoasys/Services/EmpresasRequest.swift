//
//  EmpresasRequest.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import Alamofire

public struct Constants {
    static let baseURL = URL(string: "https://empresas.ioasys.com.br")!
    static let apiPath = "api/v1/"
    static let authenticationHeaders = ["access-token", "client", "uid"]
    static let authenticationHeadersDefaultsKey = "authenticationHeaders"
}

typealias Completion<T> = (Result<T>) -> Void


func empresasRequest<T: Codable>(_ url: String, method: HTTPMethod, parameters: Parameters? = nil, headerType: HeaderType, completion: Completion<T>? = nil) {
    var headers: HTTPHeaders = []
    
    let path = Constants.baseURL.absoluteString + "/" + Constants.apiPath + url
    
    if headerType == .userCredentials {
        
        Constants.authenticationHeaders.forEach{
            if UserDefaults.standard.string(forKey: $0) != nil {
                headers[$0] = UserDefaults.standard.string(forKey: $0)
            }
        }
        AF.request(path, method: method, parameters: parameters, headers: headers).responseData { (response) in
            guard let data = response.data else {
                if let error = response.error {
                    completion?(.failure(error.localizedDescription, response.response?.statusCode ?? 0))
                } else {
                    completion?(.failure("", 0))
                }
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let object = try decoder.decode(T.self, from: data)
                completion?(.success(object))
            } catch let err {
                completion?(.failure(err.localizedDescription, 0))
            }
        }
    } else {
        AF.request(path, method: method, parameters: parameters, headers: headers).responseData { (response) in
            
            
            if let headers = response.response?.allHeaderFields {
                Constants.authenticationHeaders.forEach{
                    UserDefaults.standard.set(headers[$0], forKey: $0)
                }
            }
            
            
            guard let data = response.data else {
                if let error = response.error {
                    completion?(.failure(error.localizedDescription, response.response?.statusCode ?? 0))
                } else {
                    completion?(.failure("", 0))
                }
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let object = try decoder.decode(T.self, from: data)
                completion?(.success(object))
            } catch let err {
                completion?(.failure(err.localizedDescription, 0))
            }
        }
    }
    
    
}
