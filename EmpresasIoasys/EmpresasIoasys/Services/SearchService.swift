//
//  SearchService.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import Alamofire

public class EnterpriseService {
    enum Paths: String {
        case search = "enterprises"
    }
    
    func searchEnterprisesWithParameters(text: String, completion: Completion<Enterprises>? = nil) {
        let parameters: Parameters = [
            "name": text
        ]
        empresasRequest(Paths.search.rawValue, method: .get, parameters: parameters, headerType: .userCredentials, completion: completion)
    }
    
}
