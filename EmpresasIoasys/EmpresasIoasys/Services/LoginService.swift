//
//  LoginService.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Alamofire

public class LoginService {
    enum Paths: String {
        case login = "users/auth/sign_in"
    }
    
    func login(email: String, password: String, completion: Completion<InvestorNested>? = nil) {
        let parameters: Parameters = [
            "email": email,
            "password": password
        ]
        empresasRequest(Paths.login.rawValue, method: .post, parameters: parameters, headerType: .auth, completion: completion)
    }
    
}
