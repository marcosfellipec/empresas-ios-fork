//
//  UnityOfWork.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
public class UnityOfWork {
    
    internal static var loginService: LoginService = LoginService()
    
    internal static var enterpriseService: EnterpriseService = EnterpriseService()
    
}

