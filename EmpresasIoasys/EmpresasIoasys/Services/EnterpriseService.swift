//
//  SearchService.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

public class EnterpriseService {
    enum Paths: String {
        case search = "enterprises/"
    }
    
    func searchEnterprisesWithParameters(text: String, completion: Completion<Enterprises>? = nil) {
        let path = Paths.search.rawValue
        let parameters: Parameters = [
            "name": text]
        empresasRequest(path, method: .get, parameters: parameters, headerType: .userCredentials, completion: completion)
    }
    
}
