//
//  UIImage+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 09/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit
import Kingfisher
import SkeletonView
extension UIImageView {
    func loadUrlFrom(urlString: String, completion: ((_ success: Bool) -> Void)? = nil) {
        if urlString != "" {
            let url = URL(string: Constants.baseURL.absoluteString + urlString)
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url, options: [.transition(.fade(0.2))], completionHandler:  { (result) in
                switch result {
                case .success(_):
                    completion?(true)
                case .failure(_):
                    completion?(false)
                }
            })
        } else {
            completion?(false)
        }    }
}
