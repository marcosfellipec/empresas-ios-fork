//
//  String+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 10/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func removeWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var encodeUrl: String {
        get {
            self.trim.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self
        }
    }
    
    var trim: String {
        get {
            return self.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
}
