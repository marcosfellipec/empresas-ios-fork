//
//  UIScrollView+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 10/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import UIKit
import ESPullToRefresh

extension UITableView {
    //Pull to Refresh
    func addRefreshCompletion(completion: @escaping ESRefreshHandler) {
        self.es.addPullToRefresh(handler: completion)
        
        (self.header?.animator as? ESRefreshHeaderAnimator)?.pullToRefreshDescription = "Puxe para atualizar"
        (self.header?.animator as? ESRefreshHeaderAnimator)?.releaseToRefreshDescription = "Solte para atualizar"
        (self.header?.animator as? ESRefreshHeaderAnimator)?.loadingDescription = "Atualizando..."
    }
    
    func stopRefresh(completion: (() -> ())? = nil) {
        self.es.stopPullToRefresh()
        self.es.stopLoadingMore()
        if let refresh = self.refreshControl, refresh.isRefreshing {
            refresh.endRefreshing()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    completion()
                })
            }
        } else { completion?() }
    }
    
    func showEmptyMessage(_ message: String, imageName: String? = nil) {
        EmptyMessage.display(message: message, imageName: imageName, in: self)
    }
    
    func hideEmptyMessage() {
        if let loadingView = self.subviews.first(where: { $0.restorationIdentifier == "emptyMessage" }) {
            self.getParentViewController()?.view.sendSubviewToBack(self)
            loadingView.removeFromSuperview()
        }
    }
}
