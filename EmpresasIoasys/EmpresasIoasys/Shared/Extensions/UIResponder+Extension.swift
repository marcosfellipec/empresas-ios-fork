//
//  UIResponder+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 09/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//
import UIKit

extension UIResponder {
    func getParentViewController() -> UIViewController? {
        if self.next is UIViewController {
            return self.next as? UIViewController
        } else {
            if self.next != nil {
                return (self.next!).getParentViewController()
            }
            else {return nil}
        }
    }
}
