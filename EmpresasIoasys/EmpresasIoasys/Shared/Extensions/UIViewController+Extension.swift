//
//  UIViewController+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 10/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    @IBAction func dismissGeneric() {
        if let navigation = navigationController, self != navigation.children.first {
            navigation.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
}
