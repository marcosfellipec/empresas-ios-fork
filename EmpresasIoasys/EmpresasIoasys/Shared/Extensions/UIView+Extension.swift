//
//  UIView+Extension.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 09/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//
import Foundation
import UIKit
import KTLoadingLabel
import NVActivityIndicatorView

@IBDesignable
extension UIView {
    var loadingViewTag: Int? {
        get {
            return 100
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func pathCurvedForView(curvedPercent:CGFloat) -> UIBezierPath {
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:bounds.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:bounds.size.width, y:bounds.size.height - (bounds.size.height*curvedPercent)))
        arrowPath.addQuadCurve(to: CGPoint(x:0, y:bounds.size.height - (bounds.size.height*curvedPercent)), controlPoint: CGPoint(x:bounds.size.width/2, y:bounds.size.height))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        
        return arrowPath
    }
    
    func applyCurvedPath(curvedPercent:CGFloat) {
        guard curvedPercent <= 1 && curvedPercent >= 0 else{
            return
        }
        
        let shapeLayer = CAShapeLayer(layer: layer)
        shapeLayer.path = self.pathCurvedForView(curvedPercent: curvedPercent).cgPath
        shapeLayer.frame = bounds
        shapeLayer.masksToBounds = true
        layer.mask = shapeLayer
    }
    
    func autoHideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        endEditing(true)
    }
    
    func showLoadingView(below subview: UIView? = nil, backgroundColor: UIColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.5), activityColor: UIColor? = #colorLiteral(red: 0.9121171236, green: 0.2268618345, blue: 0.4864330888, alpha: 1)) {
        if self.viewWithTag(loadingViewTag!) == nil {
            let view = UIView()
            view.restorationIdentifier = "loadingView"
            view.backgroundColor = backgroundColor
            view.translatesAutoresizingMaskIntoConstraints = false
            
            let topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let leadingConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            
            if subview != nil {
                self.insertSubview(view, belowSubview: subview!)
            } else {
                self.addSubview(view)
                self.getParentViewController()?.view.bringSubviewToFront(self)
            }
            
            self.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
            let loadActivity = NVActivityIndicatorView(frame: CGRect(x: view.frame.midX, y: view.frame.midY, width: 40, height: 40), type: nil, color: activityColor, padding: nil)
            loadActivity.startAnimating()
            loadActivity.translatesAutoresizingMaskIntoConstraints = false
            
            let centerX = NSLayoutConstraint(item: loadActivity, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: loadActivity, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
            
            view.addSubview(loadActivity)
            view.addConstraints([centerY, centerX])
        }
    }
    
    func hideLoadingView() {
        if let loadingView = self.subviews.first(where: { $0.restorationIdentifier == "loadingView" }) {
            self.getParentViewController()?.view.sendSubviewToBack(self)
            loadingView.superview?.isUserInteractionEnabled = true
            loadingView.removeFromSuperview()
        }
    }
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
        self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 0).isActive = true
        self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: 0).isActive = true
    }
    
    func bindFrameTo(view: UIView) {
        self.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    }
    
    
}

enum Orientation {
    case vertical
    case horizontal
}
