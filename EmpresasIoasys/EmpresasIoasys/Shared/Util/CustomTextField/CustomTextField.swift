//
//  CustomTextField.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 09/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit

protocol CustomTextFieldDelegate {
    func didBeginEditing(_ textField: UITextField)
}

class CustomTextField: CustomViewDesinable {
    
    enum TextFieldStatusEnum {
        case email
        case password
    }
    
    var customTextFieldDelegate: CustomTextFieldDelegate?
    
    
    @IBInspectable var title: String = "title" {
        didSet {
            titleLabel.text = title
        }
    }
    
    @IBInspectable var text: String = "" {
        didSet {
            textField.text = text
        }
    }
    
    @IBInspectable var isSecureTextEntry: Bool = false {
        didSet {
            textField.isSecureTextEntry = isSecureTextEntry
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UIView!
    @IBOutlet weak var buttonStatus: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    private var textFieldStatus: TextFieldStatusEnum = .email
    
    
    @IBAction func buttonStatusAction(_ sender: Any) {
        if textFieldStatus == .password {
            textField.isSecureTextEntry = !textField.isSecureTextEntry
            isSecureTextEntry = textField.isSecureTextEntry
        }
        
    }
    
    override var nibName: String {
        get {
            return "CustomTextField"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        setupStatusButton()
    }
    
    
    func setupView(status: TextFieldStatusEnum) {
        textFieldStatus = status
        setupStatusButton()
    }
    
    private func setupStatusButton() {
        switch textFieldStatus {
        case .email:
            textField.isSecureTextEntry = false
            buttonStatus.isHidden = true
            contentTextField.borderWidth = 0
        case .password:
            textField.isSecureTextEntry = isSecureTextEntry
            buttonStatus.isHidden = false
            buttonStatus.setImage(#imageLiteral(resourceName: "eyes"), for: .normal)
            contentTextField.borderWidth = 0
        }
        
    }
    
    func setErrorMode() {
        textField.isSecureTextEntry = isSecureTextEntry
        buttonStatus.isHidden = false
        buttonStatus.setImage(#imageLiteral(resourceName: "errorIcon"), for: .normal)
        contentTextField.borderWidth = 1
        contentTextField.borderColor = UIColor(hexString: "EB5757")
    }
    
    func setNormalMode() {
        setupStatusButton()
    }
    
    
}


extension CustomTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        customTextFieldDelegate?.didBeginEditing(textField)
    }
}
