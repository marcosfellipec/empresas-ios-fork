//
//  EmptyMessage.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 10/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation
import UIKit

class EmptyMessage: UIView {

    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    // MARK: TableView
    static func display(message: String, imageName: String? = nil, in tableView: UITableView) {
        tableView.backgroundView = nil
        
        let emptyView = Bundle.main.loadNibNamed("EmptyMessage", owner: tableView, options: nil)?.first as? EmptyMessage
        emptyView?.frame.origin = CGPoint(x: tableView.center.x, y: tableView.center.y)
        emptyView?.message.text = message
        if let image = imageName {
            emptyView?.image.image = UIImage(named: image)
        } else {
            emptyView?.image.isHidden = true
        }
        tableView.backgroundView = emptyView
    }
    
}
