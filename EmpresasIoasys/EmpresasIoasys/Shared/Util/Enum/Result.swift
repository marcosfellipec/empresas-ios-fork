//
//  Result.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 13/10/20.
//  Copyright © 2020 Marcos Fellipe Costa Silva. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(String, Int)
    
}
