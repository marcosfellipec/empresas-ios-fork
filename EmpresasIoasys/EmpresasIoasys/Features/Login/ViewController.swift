//
//  ViewController.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 21/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var customEmailView: CustomTextField!
    @IBOutlet weak var customPasswordView: CustomTextField!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var headerImageView: UIImageView!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    private var isErrorMode = false {
        didSet {
            if isErrorMode {
                errorLabel.isHidden = false
                customEmailView.setErrorMode()
                customPasswordView.setErrorMode()
            } else {
                errorLabel.isHidden = true
                customEmailView.setNormalMode()
                customPasswordView.setNormalMode()
            }
        }
    }
    
    /**
     Realiza o login do usuário caso seus dados estejam corretos e não haja nenhum erro, caso contrário irá apresentar um UIAlert informando o tipo de erro que ocorreu durante o processo de autenticação.
     */
    @IBAction func signInAction(_ sender: Any) {
        
        view.showLoadingView()
        isErrorMode = false
        
        UnityOfWork.loginService.login(email: customEmailView.textField.text ?? "", password: customPasswordView.textField.text ?? "") { (result) in
            self.view.hideLoadingView()
            switch result {
            case .success(_):
                self.performSegue(withIdentifier: "homeSegue", sender: nil)
            case .failure(_, _):
                self.isErrorMode = true
            }
        }
        
//        AuthenticationAPI.loginWith(email: customEmailView.textField.text ?? "", password: customPasswordView.textField.text ?? "") { (response, error, cache) in
//            self.view.hideLoadingView()
//            if response != nil {
//                self.performSegue(withIdentifier: "homeSegue", sender: nil)
//            } else if let _ = error {
//                self.isErrorMode = true
//            }
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signButton.layer.cornerRadius = 6
        customEmailView.setupView(status: .email)
        customEmailView.customTextFieldDelegate = self
        customPasswordView.setupView(status: .password)
        customPasswordView.customTextFieldDelegate = self
        isErrorMode = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerImageView.applyCurvedPath(curvedPercent: 0.3)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension ViewController: CustomTextFieldDelegate {
    func didBeginEditing(_ textField: UITextField) {
        isErrorMode = false
    }
}

