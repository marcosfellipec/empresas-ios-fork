//
//  DetailViewController.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 23/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit
import SkeletonView

class DetailViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var enterprisePhoto: UIImageView!
    @IBOutlet weak var enterpriseDescription: UILabel!
    @IBOutlet weak var enterpriseNameLabel: UILabel!
    
    var enterprise: Enterprise = Enterprise()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterprisePhoto.layer.borderColor = UIColor.charcoalGrey.cgColor
        enterprisePhoto.layer.borderWidth = 0.5
        
        enterpriseDescription.text = enterprise.description
        enterprisePhoto.loadUrlFrom(urlString: enterprise.photo ?? "")
        enterpriseNameLabel.text = enterprise.enterpriseName.trim
    }
    

    
}
