//
//  EnterpryseTableViewCell.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 22/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet weak var enterprisePhoto: UIImageView!
    
    var enterprise: Enterprise = Enterprise() {
        didSet {
            enterprisePhoto.loadUrlFrom(urlString: enterprise.photo ?? "")
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
