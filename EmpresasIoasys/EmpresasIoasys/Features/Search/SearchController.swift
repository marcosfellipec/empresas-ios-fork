//
//  SearchController.swift
//  EmpresasIoasys
//
//  Created by Marcos Fellipe Costa Silva on 22/11/18.
//  Copyright © 2018 Marcos Fellipe Costa Silva. All rights reserved.
//

import UIKit
import SkeletonView
import Foundation

class SearchViewControntroller: UIViewController, UISearchBarDelegate, SkeletonTableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var enterpriseTable: UITableView!
    
    var customTransition = TransitioningDelegate()
    
    private var enterprises = [Enterprise]() {
        didSet {
            
            if enterprises.isEmpty {
                enterpriseTable.showEmptyMessage("Nenhum resultado encontrado")
            } else {
                enterpriseTable.showEmptyMessage("")
            }
            
            enterpriseTable.reloadData()
        }
    }
    
    var interactionController: UIPercentDrivenInteractiveTransition?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        modalPresentationStyle = .custom

    }
    
    
    @IBAction func TextFieldDidChange(_ sender: Any) {
        getEnterprises(text: searchTextField.text ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.autoHideKeyboard()
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Empresas",attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "666666")])
        
        enterpriseTable.addRefreshCompletion {
            self.getEnterprises(text: "")
        }
        
        getEnterprises(text: "")
        
    }
    
    // MARK: - Services
    
    func getEnterprises(text: String) {
        view.showAnimatedGradientSkeleton()
        UnityOfWork.enterpriseService.searchEnterprisesWithParameters(text: text) { (result) in
            self.view.hideSkeleton()
            self.enterpriseTable.stopRefresh()
            switch result {
            case .success(let enterprises):
            self.enterprises = enterprises.enterprises
            case .failure(_, _):
            self.enterprises = []
            }
        }
    }
    
    // MARK: - TableView
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "enterpriseCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell", for: indexPath) as? EnterpriseCell else { return UITableViewCell()}
        cell.hideSkeleton()
        cell.enterprise = enterprises[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.customPresent(enterprise: enterprises[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let sectionHeader = UIView()
            sectionHeader.backgroundColor = UIColor(hexString: "FFFFFF")
            let label = UILabel()
            label.text = "\(enterprises.count) resultados encontrados"
            label.font = label.font.withSize(14)
            label.textColor = UIColor(hexString: "666666")
            sectionHeader.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            label.trailingAnchor.constraint(equalTo: sectionHeader.trailingAnchor, constant: -15).isActive = true
            label.leadingAnchor.constraint(equalTo: sectionHeader.leadingAnchor, constant: 15).isActive = true
            label.topAnchor.constraint(equalTo: sectionHeader.topAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: sectionHeader.bottomAnchor).isActive = true
            return sectionHeader
        }
        
        return nil
    }
    
    
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailStoryboardSegue" {
            if let controller = segue.destination as? DetailViewController {
                let indexpath = sender as! IndexPath
                controller.enterprise = enterprises[indexpath.row]
                
            }
        }
    }
    
    func customPresent(enterprise: Enterprise) {
        guard let vc = UIStoryboard(name: "Detail", bundle: nil).instantiateInitialViewController() as? DetailViewController else {return}
        vc.transitioningDelegate = customTransition
        vc.enterprise = enterprise
        self.present(vc, animated: true, completion: nil)
    }
}

extension SearchViewControntroller: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}
